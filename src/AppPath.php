<?php

namespace Techendeavors\AppPath;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class AppPath
{

    /**
     * @var  string Root folder of Snap package mounts
     */
    const SNAPPATH = "/snap";

    /**
     * @var string Command to get the time zone used by the underlaying filesystem
     */
    const TZ_SHELL_EXTRACT = "date +%:z";

    /**
     * @var string TimeZone to convert filesystem datetimes to
     */
    const TZ_DEFAULT = "UTC";

    /**
     * @var string Paramaters to use with the find command when looking for the snap target match
     */
    const FIND_PARAM = " -type f -readable -executable -name ";

    /**
     * App the user is searching for
     * @var string
     */
    protected $appsearch;

    /**
     * Primary interface for searching
     * @param  string $searchString Application name you are looking for
     * @return collection               collection of paths and file information (if available)
     */
    public static function search($searchString)
    {
        if (empty($searchString)) {
            throw new Exception('You did not supply a search string');
        }

        $possibleMatches = static::searchFilesystem($searchString);

        if (empty($possibleMatches)) {
            throw new Exception('No matches for your search string were found');
        }

        $matches = static::filterUnreadable($possibleMatches);

        $results = new Collection;

        foreach ($matches as $match) {
            $fileTarget = $match;

            if (static::isSnap($match)) {
                $fileTarget = static::findSnapTarget($searchString);
            }

            if (class_exists('Techendeavors\FileInfo\FileInfo')) {
                $info = new \Techendeavors\FileInfo\FileInfo;
                $details = $info->show($fileTarget);
            } else {
                if (static::isSnap($match)) {
                    $details = static::shellFileInfo($fileTarget);
                } else {
                    $details = static::nativeFileInfo($fileTarget);
                }
            }
            $results->push(['match' => $match, 'details' => $details]);
        }

        return $results;
    }

    /**
     * Gets file information using native PHP functions
     * @param  string $target path of the file to be examined
     * @return collection         key/value collection of information
     */
    private static function nativeFileInfo($target)
    {
        return collect([
            "query" => $target,
            "tests" => [
                "directory" => false,
                "executable" => true,
                "file" => true
            ],
            "permissions" => [
                "readable" => is_readable($target),
                "writable" => is_writable($target),
                "user" => posix_getpwuid(fileowner($target))["name"],
                "group" => posix_getgrgid(filegroup($target))["name"],
                "octal" => (string) substr(decoct(fileperms($target)), 2)
            ],
            "size" => [
                "bytes" => filesize($target)
            ],
            "events" => [
                "modified" => Carbon::createFromTimestamp(filemtime($target), trim(shell_exec(static::TZ_SHELL_EXTRACT)))->setTimeZone(static::TZ_DEFAULT)
            ]            
        ]);
    }



    /**
     * Gets file information using the stat utility common on linux systems
     * @param  string $target path of the file to be examined
     * @return collection         key/value collection of information
     */
    private static function shellFileInfo($target)
    {
        $stats = array_filter(explode(",", shell_exec('stat --printf=%s,%a,%U,%G,%x,%y '.$target)));
        return collect([
            "query" => $target,
            "tests" => [
                "directory" => (bool) ! trim(shell_exec('test -d '.$target.'; echo $?')),
                "executable" => (bool) ! trim(shell_exec('test -x '.$target.'; echo $?')),
                "link" => true
            ],
            "permissions" => [
                "readable" => (bool) ! trim(shell_exec('test -r '.$target.'; echo $?')),
                "writable" => (bool) ! trim(shell_exec('test -w '.$target.'; echo $?')),
                "user" => $stats[2],
                "group" => $stats[3],
                "octal" => '0'.$stats[1]
            ],
            "size" => [
                "bytes" => $stats[0]
            ],
            "events" => [
                "modified" => Carbon::createFromFormat("Y-m-d h:i:s.u+", substr($stats[5], 0, -6), timezone_open(substr($stats[5], -5)))->setTimeZone(static::TZ_DEFAULT)
            ]
        ]);
    }

    /**
     * @param  string $appsearch
     * @throws Exception
     */
    public function __construct(string $searchString = null)
    {
        $this->searchString = $searchString;
    }

    /**
     * removes paths from the collection if the file can't be read by the process
     * @param  [type] $possibleMatches [description]
     * @return [type]                  [description]
     */
    protected static function filterUnreadable($possibleMatches)
    {
        $filtered = collect($possibleMatches)->each(function ($match) {
            return is_readable($match);
        });

        return $filtered;
    }

    /**
     * For some reason, we can't view Snap information unless its through the shell. This prevents
     * using native PHP functions
     *
     * TODO: Not sure what to do if there is more than one executable with the same name in the snap package.
     *       It'll likely require analyzing the *.wrapper configuration file
     * @param  string       The original search term
     * @return string       path to the actual executable
     */
    protected static function findSnapTarget($searchString)
    {
        return trim(shell_exec('find /snap/'.$searchString.'/current/'.static::FIND_PARAM.$searchString));
    }

    /**
     * Snap Packages are actual archives that are mounted by a process and made available to users. The downside is that
     * they are only mounted when they are accessed via the shell, so in order to get file details about something in a
     * snap package, we have to treat it special.
     * @param  string $target path to a program
     * @return boolean        returns true if the path starts with '/snap'
     */
    protected static function isSnap($path)
    {
        return (substr($path, 0, strlen(static::SNAPPATH)) == static::SNAPPATH);
    }

    protected static function searchFilesystem($searchString)
    {
        return collect(array_filter(explode("\n", shell_exec('which -a ' . escapeshellcmd($searchString)))));
    }

}
