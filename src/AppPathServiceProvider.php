<?php

namespace Techendeavors\AppPath;

use Illuminate\Support\ServiceProvider;

class AppPathServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return  void
     */
    public function boot()
    {
    }

}
