# Changelog
All notable changes to `techendeavors/app-path` will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2018-07-08
### Added
- Pass program name and receive an object of paths and file information
- Handles programs located in SNAP packages
- Data is returned as an object
- Dates are returned as Carbon objects (through Illuminate\Support\Carbon)

### Changed
- 

### Removed
- 

## [1.1.0] - 2018-07-12
### Added
- Tries to use the `Techendeavors\FileInfo` package to show file info
- Falls back on native or php functions if `FileInfo` isn't available

### Changed
- Major refactor that removed the Laravel Facade and the ability to call the package statically
- Converted a lot of functions to private

### Removed
- 
