# AppPath

## Very short description of the package

Wrapper for the linux `which` command. 

It comes in handy for people who want to automatically configure application paths in your Laravel web application. 

We've taken care to ensure that it takes a lot of edge cases into account and prevents problems.

- uses `which` which is built into the shell so it's the one thing that we can assume. 
- uses native PHP functions to get file information when possible 
- When the binary is located inside of a snap package, uses the shell to get the file information
- verifies that the user that owns the PHP process can actually execute the found applications
- If multiple runnable versions are found, sorts the results according to the latest modification date
- Deals with servers that have a different timezones for their filesystem and the PHP process

[![Latest Version on Packagist](https://img.shields.io/packagist/v/techendeavors/app-path.svg?style=flat-square)](https://packagist.org/packages/techendeavors/app-path)
[![Total Downloads](https://img.shields.io/packagist/dt/techendeavors/app-path.svg?style=flat-square)](https://packagist.org/packages/techendeavors/app-path)

### To Do

- [ ] Use config file to allow people to override how results should be sorted
- [ ] Use config file to add additional locations to the PATH
- [X] Create the data as an object from the start rather than converting everything to an object
- [ ] Verify `stat` exists before calling it
- [X] If file is not a `snap` package then use native PHP functions to get file data
- [x] Migrate to using the Techendeavors\FileInfo package once it is available
- [X] Break out some components into individual functions to clean everything up
- [X] Keep date as a Carbon object
- [ ] Make sure it works on multiple platforms, including Windows and MacOS
- [ ] Write Tests
- [ ] Make a helper
- [X] Allow it to be called statically
- [ ] Create a __string function that just outputs the path of the top choice
- [ ] Sort collection by modification date

### Installation

You can install the package via composer:

```bash
composer require techendeavors/app-path
```

### Usage


``` 
>>> AppPath::search('ffmpeg')
=> Illuminate\Support\Collection {#2843
     all: [
       [
         "match" => "/usr/bin/ffmpeg",
         "details" => Illuminate\Support\Collection {#2840
           all: [
             "query" => "/usr/bin/ffmpeg",
             "tests" => [
               "directory" => false,
               "executable" => true,
               "file" => true,
               "link" => false,
             ],
             "permissions" => [
               "readable" => true,
               "writable" => false,
               "user" => "root",
               "group" => "root",
               "octal" => "0755",
               "human" => "rwxr-xr-x",
             ],
             "info" => [
               "name" => "ffmpeg",
               "path" => "/usr/bin",
               "type" => "file",
               "mime" => "application/x-sharedlib",
             ],
             "size" => [
               "bytes" => 272528,
               "human" => "266.14KB",
             ],
             "hashes" => null,
             "events" => [
               "modified" => Carbon\Carbon @1523273080 {#2849
                 date: 2018-04-09 11:24:40.0 UTC (+00:00),
               },
               "changed" => Carbon\Carbon @1526714923 {#2850
                 date: 2018-05-19 07:28:43.0 UTC (+00:00),
               },
               "accessed" => Carbon\Carbon @1531345222 {#2851
                 date: 2018-07-11 21:40:22.0 UTC (+00:00),
               },
             ],
           ],
         },
       ],
       [
         "match" => "/snap/bin/ffmpeg",
         "details" => Illuminate\Support\Collection {#2846
           all: [
             "query" => "/snap/ffmpeg/current/usr/bin/ffmpeg",
             "tests" => [
               "directory" => false,
               "executable" => true,
               "file" => true,
               "link" => false,
             ],
             "permissions" => [
               "readable" => true,
               "writable" => false,
               "user" => "root",
               "group" => "root",
               "octal" => "0755",
               "human" => "rwxr-xr-x",
             ],
             "info" => [
               "name" => "ffmpeg",
               "path" => "/snap/ffmpeg/current/usr/bin",
               "type" => "file",
               "mime" => "application/x-executable",
             ],
             "size" => [
               "bytes" => 259304,
               "human" => "253.23KB",
             ],
             "hashes" => null,
             "events" => [
               "modified" => Carbon\Carbon @1526035316 {#2853
                 date: 2018-05-11 10:41:56.0 UTC (+00:00),
               },
               "changed" => Carbon\Carbon @1526035316 {#2854
                 date: 2018-05-11 10:41:56.0 UTC (+00:00),
               },
               "accessed" => Carbon\Carbon @1526035316 {#2855
                 date: 2018-05-11 10:41:56.0 UTC (+00:00),
               },
             ],
           ],
         },
       ],
     ],
   }
>>> 

```

### Testing

Not yet implemented

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

### Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please use the [issue tracker](https://gitlab.com/techendeavors/app-path)

### Credits

- [Techendeavors](https://gitlab.com/techendeavors)
- [Contributors and Credits](CONTRIBUTORS.md)

### License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
